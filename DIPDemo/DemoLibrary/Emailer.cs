﻿using System;

namespace DemoLibrary
{
    public class Emailer : IMessageSender
    {
        public void SendMessage(IPerson person, string message)
        {
            Console.WriteLine($"Sending mail to {person.EmailAddress}: {message}");
        }
    }
}
