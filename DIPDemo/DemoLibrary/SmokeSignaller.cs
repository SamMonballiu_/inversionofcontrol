﻿using System;

namespace DemoLibrary
{
    public class SmokeSignaller : IMessageSender
    {
        public void SendMessage(IPerson person, string message)
        {
            Console.WriteLine($"Sending smoke signals to {person.FirstName} {person.LastName}: {message}");
        }
    }
}
