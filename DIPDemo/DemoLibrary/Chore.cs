﻿using System.Collections.Generic;

namespace DemoLibrary
{
    public class Chore : IChore
    {
        public string ChoreName { get; set; }
        public IPerson Owner { get; set; }
        public double HoursWorked { get; set; }
        public bool IsComplete { get; set; }

        #region Constructor injection

        // Instead of relying on new keyword to make new Loggers or MessageSenders,
        // declare two fields using the respective interfaces,
        // and initialize them in the constructor.

        private ILogger _logger;
        private IMessageSender _messageSender;

        private IMessageSender[] _messageMultipleSenders;

        public Chore(ILogger logger, IMessageSender messageSender)
        {
            _logger = logger;
            _messageSender = messageSender;
        }

        public Chore(ILogger logger, IMessageSender[] messageSenders)
        {
            _logger = logger;
            _messageMultipleSenders = messageSenders;
        }

        #endregion

        public void PerformWork(double hours)
        {
            HoursWorked += hours;

            _logger.Log($"Spent {hours}h on '{ChoreName}'.");
        }

        public void CompleteChore()
        {
            IsComplete = true;

            _logger.Log($"Completed '{ChoreName}'.");

            if (_messageSender != null)
            {
                _messageSender.SendMessage(Owner, $"Completed '{ChoreName}'.");
            }

            if (_messageMultipleSenders != null)
            {
                foreach (var item in _messageMultipleSenders)
                {
                    item.SendMessage(Owner, $"Completed '{ChoreName}'.");
                }
            }
        }
    }
}
