﻿using DemoLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InversionOfControl
{
    public static class Factory
    {
        // instead of having 'new' keyword in a class when a person needs to be made, 
        // have the class refer to this method, so the new keyword is relegated only to this method
        // its return type is not a Person, but an ** IPerson **
        // not a type, but the interface IMPLEMENTED by the type
        public static IPerson CreatePerson()
        {
            return new Person();
        }

        public static IChore CreateChore()
        {
            return new Chore(CreateLogger(), CreateTexter());
        }

        public static IChore CreateChoreMultipleSenders(params IMessageSender[] senders)
        {
            return new Chore(CreateLogger(), senders);
        }

        public static ILogger CreateLogger()
        {
            return new Logger();
        }

        public static IMessageSender CreateEmailer()
        {
            return new Emailer();
        }

        public static IMessageSender CreateTexter()
        {
            return new Texter();
        }

        public static IMessageSender CreateSmokeSignaller()
        {
            return new SmokeSignaller();
        }

    }
}
