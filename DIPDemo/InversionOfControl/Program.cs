﻿using DemoLibrary;
using System;
using Unity;

namespace InversionOfControl
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = new UnityContainer();

            container.RegisterType<IPerson, Person>();
            container.RegisterType<IChore, Chore>();
            container.RegisterType<IMessageSender, Emailer>();
            container.RegisterType<ILogger, Logger>();

            //IPerson owner = Factory.CreatePerson();
            var owner = container.Resolve<IPerson>();
            owner.FirstName = "Kenan";
            owner.LastName = "Kurda";
            owner.PhoneNumber = "0032472606060";
            owner.EmailAddress = "kenankurda@gmail.com";

            var chore = container.Resolve<Chore>();


            //IChore chore = Factory.CreateChore();

            //chore = Factory.CreateChoreMultipleSenders(
            //    Factory.CreateEmailer(),
            //    Factory.CreateTexter(),
            //    Factory.CreateSmokeSignaller()
            //    );

            chore.ChoreName = "Programming on a new game";
            chore.Owner = owner;

            chore.PerformWork(3);
            chore.PerformWork(1.5);
            chore.CompleteChore();

            Console.ReadKey();
        }

        static UnityContainer GetContainer()
        {
            var container = new UnityContainer();

            container.RegisterType<IPerson, Person>();
            container.RegisterType<IChore, Chore>();
            container.RegisterType<IMessageSender, Emailer>();

            return container;
        }
    }
}
